-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema ticketbookingsystem
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ticketbookingsystem
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ticketbookingsystem` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `ticketbookingsystem` ;

-- -----------------------------------------------------
-- Table `ticketbookingsystem`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ticketbookingsystem`.`user` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(50) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_bin' NULL DEFAULT NULL,
  `firstName` VARCHAR(50) NOT NULL,
  `lastName` VARCHAR(100) NOT NULL,
  `phoneNumber` VARCHAR(50) NOT NULL,
  `password` VARCHAR(50) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_bin' NULL DEFAULT NULL,
  `type` ENUM('Admin', 'Employee', 'Customer', 'Artist') NULL DEFAULT NULL,
  `status` ENUM('Active', 'Inactive', 'Banned') NOT NULL,
  `address1` VARCHAR(150) NOT NULL,
  `address2` VARCHAR(150) NOT NULL,
  `City` VARCHAR(50) NOT NULL,
  `County` VARCHAR(50) NOT NULL,
  `Country` VARCHAR(50) NOT NULL,
  `PostCode` VARCHAR(20) NOT NULL,
  `profileActive` BIT(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `userID_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `userEmail_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ticketbookingsystem`.`artist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ticketbookingsystem`.`artist` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `contactNumber` VARCHAR(50) NOT NULL,
  `artisticName` VARCHAR(15) NOT NULL,
  `eventType` VARCHAR(50) NOT NULL,
  `user_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `artistID_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_artist_user_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_artist_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `ticketbookingsystem`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ticketbookingsystem`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ticketbookingsystem`.`customer` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `dob` DATE NOT NULL,
  `gender` ENUM('Female', 'Male', 'Other') NOT NULL,
  `user_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `customerID_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_customer_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_customer_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `ticketbookingsystem`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ticketbookingsystem`.`employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ticketbookingsystem`.`employee` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `dob` DATE NOT NULL,
  `gender` ENUM('Female', 'Male', 'Other') NOT NULL,
  `user_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `employeeID_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_employee_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_employee_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `ticketbookingsystem`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ticketbookingsystem`.`venue`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ticketbookingsystem`.`venue` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NOT NULL,
  `description` VARCHAR(250) NULL DEFAULT NULL,
  `ownerName` VARCHAR(250) NOT NULL,
  `phoneNumber` VARCHAR(50) NOT NULL,
  `address1` VARCHAR(50) NOT NULL,
  `address2` VARCHAR(50) NOT NULL,
  `EIRCODE` VARCHAR(15) NOT NULL,
  `capacity` INT NOT NULL,
  `cost` DECIMAL(10,2) NOT NULL,
  `employee_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `venueID_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_venue_employee1_idx` (`employee_id` ASC) VISIBLE,
  CONSTRAINT `fk_venue_employee1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `ticketbookingsystem`.`employee` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ticketbookingsystem`.`event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ticketbookingsystem`.`event` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `type` ENUM('Music Festival', 'Music Concert', 'Folk Music', 'Irish Music', 'Jazz', 'Country Music', 'Disco', 'Football Match', 'Rugby', 'GAA', 'Basketball', 'Arts Exhibitions') NOT NULL,
  `date` DATETIME NOT NULL,
  `adultTicketPrice` DECIMAL(10,2) NOT NULL,
  `KIDSTicketPrice` DECIMAL(10,2) NOT NULL,
  `teenagerTicketPrice` DECIMAL(10,2) NOT NULL,
  `studentTicketPrice` DECIMAL(10,2) NOT NULL,
  `elderlyTicketPrice` DECIMAL(10,2) NOT NULL,
  `category` VARCHAR(50) NOT NULL,
  `artist_id` BIGINT NOT NULL,
  `venue_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `eventID_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_event_artist1_idx` (`artist_id` ASC) VISIBLE,
  INDEX `fk_event_venue1_idx` (`venue_id` ASC) VISIBLE,
  CONSTRAINT `fk_event_artist1`
    FOREIGN KEY (`artist_id`)
    REFERENCES `ticketbookingsystem`.`artist` (`id`),
  CONSTRAINT `fk_event_venue1`
    FOREIGN KEY (`venue_id`)
    REFERENCES `ticketbookingsystem`.`venue` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ticketbookingsystem`.`payment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ticketbookingsystem`.`payment` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `type` ENUM('Card', 'Bank Order', 'Cash') NULL DEFAULT NULL,
  `date` DATE NULL DEFAULT NULL,
  `total` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `paymentID_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ticketbookingsystem`.`ticket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ticketbookingsystem`.`ticket` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `price` DECIMAL(10,2) NOT NULL,
  `type` VARCHAR(50) NOT NULL,
  `ticketQuantity` INT NOT NULL,
  `ticketPrinted` BIT(1) NOT NULL DEFAULT b'0',
  `payment_id` BIGINT NULL DEFAULT NULL,
  `event_id` BIGINT NOT NULL,
  `customer_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `ticketID_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_ticket_payment1_idx` (`payment_id` ASC) VISIBLE,
  INDEX `fk_ticket_event1_idx` (`event_id` ASC) VISIBLE,
  INDEX `fk_ticket_customer1_idx` (`customer_id` ASC) VISIBLE,
  CONSTRAINT `fk_ticket_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `ticketbookingsystem`.`customer` (`id`),
  CONSTRAINT `fk_ticket_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `ticketbookingsystem`.`event` (`id`),
  CONSTRAINT `fk_ticket_payment1`
    FOREIGN KEY (`payment_id`)
    REFERENCES `ticketbookingsystem`.`payment` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 34
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
