/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.repository;

import com.tbcommons.dto.UserDTO;
import com.tbcommons.model.Artist;
import com.tbcommons.model.Employee;
import com.tbcommons.model.User;
import java.util.List;

public interface UserRepository {

    List<User> getAll();

    UserDTO getById(Long id);

    UserDTO save(User user);

    boolean delete(Long id);

    UserDTO update(User user);

    UserDTO updateEmployee(Long userId, Employee user);

    UserDTO updateArtist(Long id, Artist artist);

    UserDTO login(String encrypted);
    
    String activeUserByEmail(String encrypted);
}
