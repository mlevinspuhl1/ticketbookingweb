/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.repository;

import com.tbcommons.dto.TicketDTO;
import com.tbcommons.dto.TicketPaymentDTO;
import com.tbcommons.model.Payment;
import com.tbcommons.model.Ticket;
import java.util.List;

/**
 *
 * @author
 */
public interface TicketRepository {

    List<Ticket> getAll();

    TicketDTO getById(Long id);

    TicketDTO save(Ticket ticket);

    TicketDTO saveAll(TicketPaymentDTO dto);

    TicketDTO update(Ticket ticket);
    
    public TicketDTO updatePrintedList(List<Long> ids);

    boolean delete(Long id);
}
