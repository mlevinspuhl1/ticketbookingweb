/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.repository;

import com.tbcommons.dto.VenueDTO;
import com.tbcommons.model.Venue;
import java.util.List;

public interface VenueRepository {

    List<Venue> getAll();

    VenueDTO getById(Long id);

    VenueDTO save(Venue venue);

    VenueDTO update(Venue venue);

    boolean delete(Long id);

}
