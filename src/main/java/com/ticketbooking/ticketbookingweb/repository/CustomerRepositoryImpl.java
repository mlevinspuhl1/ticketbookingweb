/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.repository;

import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.CustomerDTO;
import static com.tbcommons.enums.Enums.ResponseEnum.*;
import com.tbcommons.model.Customer;
import com.tbcommons.model.Ticket;
import static com.ticketbooking.ticketbookingweb.repository.Repository.Log;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.servlet.ServletContext;
import javax.validation.ValidationException;

/**
 *
 * @author
 */
public class CustomerRepositoryImpl extends Repository implements CustomerRepository {

    private static CustomerRepositoryImpl singleinstance = null;

    public static CustomerRepositoryImpl getInstance(ServletContext context) {
        if (singleinstance == null) {
            singleinstance = new CustomerRepositoryImpl(context);
        }
        return singleinstance;
    }

    private CustomerRepositoryImpl(ServletContext context) {
        emf = (EntityManagerFactory) context.getAttribute("emf");
    }

    @Override
    public List<Customer> getAll() {
        List<Customer> aList = null;
        try {
            em = emf.createEntityManager();

            TypedQuery<Customer> q = em.createQuery("SELECT c from "
                    + Customer.class.getSimpleName() + " c", Customer.class);

            aList = q.setMaxResults(100).getResultList();

        } catch (Exception ex) {
            Log.error("getAll", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return aList;
    }

    @Override
    public CustomerDTO getById(Long id) {
        try {
            if (id == null) {
                throw new ValidationException("Customer id null");
            }
            em = emf.createEntityManager();
            Customer customer = em.find(Customer.class, id);
            List<Ticket> ticketList = customer.getTicketList();

            return new CustomerDTO(SUCCESS, SUCCESS.label,
                    customer, ticketList);
        } catch (ValidationException ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("getById", ex);
            return new CustomerDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("getById", ex);
            return new CustomerDTO(FAIL, LOGIN_TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public CustomerDTO save(Customer customer) {
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            em.persist(customer);
            tx.commit();

            return new CustomerDTO(SUCCESS, SUCCESS.label, customer);
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("save", ex);
            return new CustomerDTO(FAIL, TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public CustomerDTO update(Customer customer) {

        try {
            if (customer == null) {
                throw new ValidationException("Customer is null");
            }
            if (customer.getId() == null) {
                throw new ValidationException("Customer id is null");
            }
            CustomerDTO dto = getById(customer.getId());

            if (dto == null || dto.getCustomer() == null) {
                throw new ValidationException("Customer not found by id: " + customer.getId());
            }

            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            customer = em.merge(customer);
            tx.commit();
            return new CustomerDTO(SUCCESS, SUCCESS.label, customer);
        } catch (ValidationException ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("update", ex);
            return new CustomerDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("update", ex);
            return new CustomerDTO(FAIL, CONTACT_ADMIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean delete(Long id) {
        try {
            CustomerDTO dto = getById(id);

            if (dto == null || dto.getCustomer() == null) {
                throw new Exception("Customer not found by id:" + id);
            }
            Customer customer = dto.getCustomer();
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            if (!em.contains(customer)) {
                Customer current = em.merge(customer);
                em.remove(current);
            } else {
                em.remove(customer);
            }
            tx.commit();
            return true;
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("delete", ex);
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
