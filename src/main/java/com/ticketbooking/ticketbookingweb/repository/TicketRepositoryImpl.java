/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.repository;

import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.TicketDTO;
import com.tbcommons.dto.TicketPaymentDTO;
import static com.tbcommons.enums.Enums.ResponseEnum.*;
import com.tbcommons.model.Customer;
import com.tbcommons.model.Event;
import com.tbcommons.model.Payment;
import com.tbcommons.model.Ticket;
import static com.ticketbooking.ticketbookingweb.repository.Repository.Log;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.servlet.ServletContext;
import javax.validation.ValidationException;

/**
 *
 * @author
 */
public class TicketRepositoryImpl extends Repository implements TicketRepository {

    private static TicketRepositoryImpl singleinstance = null;

    public static TicketRepositoryImpl getInstance(ServletContext context) {
        if (singleinstance == null) {
            singleinstance = new TicketRepositoryImpl(context);
        }
        return singleinstance;
    }

    private TicketRepositoryImpl(ServletContext context) {
        emf = (EntityManagerFactory) context.getAttribute("emf");
    }

    @Override
    public List<Ticket> getAll() {
        List<Ticket> aList = null;
        try {
            em = emf.createEntityManager();

            TypedQuery<Ticket> q = em.createQuery("SELECT t from " + Ticket.class.getSimpleName() + " t", Ticket.class);

            aList = q.setMaxResults(100).getResultList();

        } catch (Exception ex) {
            Log.error("getAll", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return aList;
    }

    @Override
    public TicketDTO getById(Long id) {
        try {
            if (id == null) {
                throw new ValidationException("Ticket id null");
            }
            em = emf.createEntityManager();
            Ticket ticket = em.find(Ticket.class, id);
            return new TicketDTO(SUCCESS, SUCCESS.label, ticket);
        } catch (ValidationException ex) {
            Log.error("getById", ex);
            return new TicketDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            Log.error("getById", ex);
            return new TicketDTO(FAIL, LOGIN_TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TicketDTO save(Ticket ticket) {
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            Customer customer = em.find(Customer.class, ticket.getCustomerId().getId());
            Event event = em.find(Event.class, ticket.getEventId().getId());
            List<Ticket> ticketList1 = customer.getTicketList();
            List<Ticket> ticketList2 = event.getTicketList();
            ticketList1.add(ticket);
            ticketList2.add(ticket);
            customer.setTicketList(ticketList1);
            event.setTicketList(ticketList2);
            em.persist(ticket);
            em.merge(customer);
            em.merge(event);
            em.flush();
            em.clear();
            tx.commit();
            return new TicketDTO(SUCCESS, SUCCESS.label, ticket);
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("save", ex);
            return new TicketDTO(FAIL, TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TicketDTO update(Ticket ticket) {

        try {
            if (ticket == null) {
                throw new ValidationException("Ticket is null");
            }
            if (ticket.getId() == null) {
                throw new ValidationException("Ticket id is null");
            }
            TicketDTO dto = getById(ticket.getId());

            if (dto == null || dto.getTicket() == null) {
                throw new ValidationException("Ticket not found by id: " + ticket.getId());
            }

            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            ticket = em.merge(ticket);
            tx.commit();
            return new TicketDTO(SUCCESS, SUCCESS.label, ticket);
        } catch (ValidationException ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("update", ex);
            return new TicketDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("update", ex);
            return new TicketDTO(FAIL, CONTACT_ADMIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean delete(Long id) {
        try {
            if (id == null) {
                throw new Exception("Ticket id mull");
            }
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            Ticket ticket = em.find(Ticket.class, id);
            if (ticket == null) {
                throw new Exception("Ticket not found by id:" + id);
            }
            Event event = ticket.getEventId();
            Customer customer = ticket.getCustomerId();
            event.getTicketList().remove(ticket);
            customer.getTicketList().remove(ticket);
            em.merge(event);
            em.merge(customer);
            if (!em.contains(ticket)) {
                Ticket current = em.merge(ticket);
                em.remove(current);
            } else {
                em.remove(ticket);
            }
            tx.commit();
            return true;
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("delete", ex);
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     *
     * @param dto
     * @return
     */
    @Override
    public TicketDTO saveAll(TicketPaymentDTO dto) {
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            Payment payment = dto.getPayment();
            List<Ticket> ticketList = dto.getTicketList();
            payment.setTicketList(ticketList);
            for (Ticket ticket : ticketList) {
                ticket.setPaymentId(payment);
            }

            for (Ticket ticket : ticketList) {
                Customer customer = em.find(Customer.class, ticket.getCustomerId().getId());
                Event event = em.find(Event.class, ticket.getEventId().getId());
                List<Ticket> ticketList1 = customer.getTicketList();
                List<Ticket> ticketList2 = event.getTicketList();
                ticketList1.add(ticket);
                ticketList2.add(ticket);
                customer.setTicketList(ticketList1);
                event.setTicketList(ticketList2);
                em.merge(ticket);
                em.merge(customer);
                em.merge(event);
            }
            em.persist(payment);
            em.flush();
            em.clear();
            tx.commit();
            return new TicketDTO(SUCCESS, SUCCESS.label, ticketList);
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("save", ex);
            return new TicketDTO(FAIL, TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TicketDTO updatePrintedList(List<Long> ids) {
        try {
            if (ids == null) {
                throw new ValidationException("ids null");
            }
            if (ids.isEmpty()) {
                throw new ValidationException("ids empty");
            }
            List<Ticket> ticketList = new ArrayList<>();
            for (Long id : ids) {
                TicketDTO dto = getById(id);

                if (dto == null || dto.getTicket() == null) {
                    throw new ValidationException("Ticket not found by id: " + id);
                }
                ticketList.add(dto.getTicket());
            }
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            for (Ticket ticket : ticketList) {
                ticket.setTicketPrinted(true);
                ticket = em.merge(ticket);
            }
            tx.commit();
            return new TicketDTO(SUCCESS, SUCCESS.label, ticketList);
        } catch (ValidationException ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("updatePrintedList", ex);
            return new TicketDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("updatePrintedList", ex);
            return new TicketDTO(FAIL, CONTACT_ADMIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
