/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.repository;

import static com.tbcommons.constant.Constants.*;
import static com.tbcommons.enums.Enums.ResponseEnum.*;
import static com.tbcommons.enums.Enums.UserStatusEnum.*;
import com.tbcommons.dto.UserDTO;
import com.tbcommons.model.Artist;
import com.tbcommons.model.Customer;
import com.tbcommons.model.Employee;
import com.tbcommons.model.User;
import com.ticketbooking.ticketbookingweb.service.AuthenticationService;
import com.ticketbooking.ticketbookingweb.validation.UserValidation;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.servlet.ServletContext;
import javax.validation.ValidationException;

/**
 *
 */
public class UserRepositoryImpl extends Repository implements UserRepository {

    private static UserRepositoryImpl singleinstance = null;

    public static UserRepositoryImpl getInstance(ServletContext context) {
        if (singleinstance == null) {
            singleinstance = new UserRepositoryImpl(context);
        }
        return singleinstance;
    }

    private UserRepositoryImpl(ServletContext context) {
        emf = (EntityManagerFactory) context.getAttribute("emf");
    }

    @Override
    public List<User> getAll() {
        List<User> uList = null;
        try {
            em = emf.createEntityManager();

            TypedQuery<User> q = em.createQuery("SELECT u from User u", User.class);

            uList = q.setMaxResults(100).getResultList();

        } catch (Exception ex) {
            Log.error("getUser", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return uList;
    }

    @Override
    public UserDTO getById(Long id) {

        try {
            em = emf.createEntityManager();
            User user = em.find(User.class, id);
            return new UserDTO(SUCCESS, SUCCESS.label, user,
                    user.getArtistList().isEmpty() ? null
                    : user.getArtistList().get(0),
                    user.getCustomerList().isEmpty() ? null
                    : user.getCustomerList().get(0),
                    user.getEmployeeList().isEmpty() ? null
                    : user.getEmployeeList().get(0));
        } catch (Exception ex) {
            Log.error("getById", ex);
            return new UserDTO(FAIL, LOGIN_TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private List<User> getUser(EntityManager em, String email, String password) {
        StringBuilder sb = new StringBuilder("SELECT u FROM User u ")
                .append("WHERE u.email = :email");
        if (password != null) {
            sb.append(" and u.password = :password ");
        }
        TypedQuery<User> query = em.createQuery(
                sb.toString(), User.class);

        query = query.setParameter("email", email);
        if (password != null) {
            query = query.setParameter("password", password);
        }

        return query.getResultList();
    }

    @Override
    public UserDTO login(String encrypted) {

        try {
            StringTokenizer tokenizer = AuthenticationService.getInstance()
                    .getUserCredential(encrypted);
            final String email = tokenizer.nextToken();
            final String password = tokenizer.nextToken();
            em = emf.createEntityManager();
            List<User> users = getUser(em, email, password);
            if (users.isEmpty()) {
                return new UserDTO(FAIL,
                        INVALID_EMAIL_PASSWORD_LOGIN);
            }
            User user = users.get(0);
            if (!user.getStatus().equals(ACTIVE.label)) {
                return new UserDTO(FAIL,
                        "User is " + user.getStatus().toLowerCase());
            }
            return new UserDTO(SUCCESS, SUCCESS.label, user,
                    user.getArtistList().isEmpty() ? null
                    : user.getArtistList().get(0),
                    user.getCustomerList().isEmpty() ? null
                    : user.getCustomerList().get(0),
                    user.getEmployeeList().isEmpty() ? null
                    : user.getEmployeeList().get(0));
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("login", ex);
            return new UserDTO(FAIL, LOGIN_TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public UserDTO save(User user) {

        if (!UserValidation.isEmailValid(user)) {

            return new UserDTO(FAIL, INVALID_EMAIL);
        }
        try {
            em = emf.createEntityManager();

            List<User> users = getUser(em, user.getEmail(), null);

            if (users != null && users.size() > 0) {
                return new UserDTO(FAIL, ALREADY_EXISTS_EMAIL);
            }
            tx = em.getTransaction();
            tx.begin();
            em.persist(user);
            em.flush();
            em.clear();
            tx.commit();

            return new UserDTO(SUCCESS, SUCCESS.label, user);
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("save", ex);
            return new UserDTO(FAIL, TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    @Override
    public boolean delete(Long id
    ) {
        try {
            UserDTO dto = getById(id);

            if (dto == null || dto.getUser() == null) {
                throw new Exception("User not found by id:" + id);
            }
            User user = dto.getUser();
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            if (!em.contains(user)) {
                User current = em.merge(user);
                em.remove(current);
            } else {
                em.remove(user);
            }
            tx.commit();
            return true;
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("delete", ex);
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    @Override
    public UserDTO update(User user
    ) {

        try {
            if (user == null) {
                throw new ValidationException("User null");
            }
            UserDTO dto = getById(user.getId());

            if (dto == null || dto.getUser() == null) {
                throw new ValidationException("User not found by id:" + user.getId());
            }
            User compareUser = dto.getUser();
            if (user.getStatus() == null) {
                user.setStatus(compareUser.getStatus());
            }
            if (user.getType() == null) {
                user.setType(compareUser.getType());
            }
            user.setArtistList(compareUser.getArtistList());
            user.setCustomerList(compareUser.getCustomerList());
            user.setEmployeeList(compareUser.getEmployeeList());
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            user = em.merge(user);
            tx.commit();
            return new UserDTO(SUCCESS, SUCCESS.label, user,
                    user.getArtistList().isEmpty() ? null
                    : user.getArtistList().get(0),
                    user.getCustomerList().isEmpty() ? null
                    : user.getCustomerList().get(0),
                    user.getEmployeeList().isEmpty() ? null
                    : user.getEmployeeList().get(0));
        } catch (ValidationException ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("update", ex);
            return new UserDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("update", ex);
            return new UserDTO(FAIL, CONTACT_ADMIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    @Override
    public String activeUserByEmail(String encrypted) {

        try {
            StringTokenizer tokenizer = AuthenticationService.getInstance()
                    .getUserCredential(encrypted);
            final String email = tokenizer.nextToken();
            final String password = tokenizer.nextToken();
            em = emf.createEntityManager();
            List<User> users = getUser(em, email, password);

            if (users.isEmpty()) {
                Log.error("activeUserByEmail email not found - " + email);
                return "<h1>Email not found - " + email + "</h1>";
//                return new UserDTO(FAIL.label,
//                        INVALID_EMAIL_PASSWORD_LOGIN);
            }
            User user = users.get(0);
            if (!user.getStatus().equals(ACTIVE.label)) {
                user.setStatus(ACTIVE.label);
                tx = em.getTransaction();
                tx.begin();
                user = em.merge(user);
                tx.commit();
            }
            Log.info("activeUserByEmail email activated - " + email);
            return "<h1>User activated!</h1>";
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("activeUserByEmail", ex);
            return "<h1>Please try again later!</h1>";
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public UserDTO updateEmployee(Long userId, Employee emp) {
        try {
            if (emp == null) {
                throw new ValidationException("Employee null");
            }
            UserDTO dto = getById(userId);

            if (dto == null || dto.getUser() == null) {
                throw new ValidationException("User not found");
            }
            User user = dto.getUser();
            List<Employee> empList = new ArrayList<>();
            emp.setUserId(user);
            empList.add(emp);
            user.setEmployeeList(empList);
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            user = em.merge(user);
            tx.commit();
            return new UserDTO(SUCCESS, SUCCESS.label, user,
                    user.getArtistList().isEmpty() ? null
                    : user.getArtistList().get(0),
                    user.getCustomerList().isEmpty() ? null
                    : user.getCustomerList().get(0),
                    user.getEmployeeList().isEmpty() ? null
                    : user.getEmployeeList().get(0));
        } catch (ValidationException ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("updateEmployee", ex);
            return new UserDTO(FAIL, ex.getMessage() + " id:" + userId.toString());
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("updateEmployee", ex);
            return new UserDTO(FAIL, CONTACT_ADMIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public UserDTO updateArtist(Long id, Artist artist) {
        try {
            if (artist == null) {
                throw new ValidationException("Artist null");
            }
            UserDTO dto = getById(id);

            if (dto == null || dto.getUser() == null) {
                throw new ValidationException("Artist not found");
            }
            User user = dto.getUser();
            List<Artist> artistList = new ArrayList<>();
            artist.setUserId(user);
            artistList.add(artist);
            user.setArtistList(artistList);
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            user = em.merge(user);
            tx.commit();
            return new UserDTO(SUCCESS, SUCCESS.label, user,
                    user.getArtistList().isEmpty() ? null
                    : user.getArtistList().get(0),
                    user.getCustomerList().isEmpty() ? null
                    : user.getCustomerList().get(0),
                    user.getEmployeeList().isEmpty() ? null
                    : user.getEmployeeList().get(0));
        } catch (ValidationException ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("updateArtist", ex);
            return new UserDTO(FAIL, ex.getMessage() + " id:" + id.toString());
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("updateArtist", ex);
            return new UserDTO(FAIL, CONTACT_ADMIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    public UserDTO updateCustomer(Long id, Customer customer) {
                try {
            if (customer == null) {
                throw new ValidationException("Customer null");
            }
            UserDTO dto = getById(id);

            if (dto == null || dto.getUser() == null) {
                throw new ValidationException("Customer not found");
            }
            User user = dto.getUser();
            List<Customer> customerList = new ArrayList<>();
            customer.setUserId(user);
            customerList.add(customer);
            user.setCustomerList(customerList);
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            user = em.merge(user);
            tx.commit();
            return new UserDTO(SUCCESS, SUCCESS.label, user,
                    user.getArtistList().isEmpty() ? null
                    : user.getArtistList().get(0),
                    user.getCustomerList().isEmpty() ? null
                    : user.getCustomerList().get(0),
                    user.getEmployeeList().isEmpty() ? null
                    : user.getEmployeeList().get(0));
        } catch (ValidationException ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("updateCustomer", ex);
            return new UserDTO(FAIL, ex.getMessage() + " id:" + id.toString());
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("updateCustomer", ex);
            return new UserDTO(FAIL, CONTACT_ADMIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
