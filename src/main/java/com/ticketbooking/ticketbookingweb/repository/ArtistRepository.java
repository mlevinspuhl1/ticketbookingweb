/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.repository;

import com.tbcommons.dto.ArtistDTO;
import com.tbcommons.model.Artist;
import java.util.List;

/**
 *
 * @author genef
 */
public interface ArtistRepository {
    
    List<Artist> getAll();

    ArtistDTO getById(Long id);

    ArtistDTO save(Artist artist);

    ArtistDTO update(Artist artist);

    boolean delete(Long id);
}
