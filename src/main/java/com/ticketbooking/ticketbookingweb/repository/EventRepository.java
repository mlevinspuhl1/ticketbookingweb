/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.repository;

import com.tbcommons.dto.EventDTO;
import com.tbcommons.model.Event;
import java.util.List;

/**
 *
 * @author
 */
public interface EventRepository {

    List<Event> getAll();

    EventDTO getById(Long id);

    EventDTO save(Event event);

    EventDTO update(Event event);

    boolean delete(Long id);
    
    public List<Event> getByArtistID(Long id);
}
