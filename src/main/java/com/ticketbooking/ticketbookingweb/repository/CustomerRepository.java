/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.repository;

import com.tbcommons.dto.CustomerDTO;
import com.tbcommons.model.Customer;
import java.util.List;

/**
 *
 * @author
 */
public interface CustomerRepository {

    List<Customer> getAll();

    CustomerDTO getById(Long id);

    CustomerDTO save(Customer artist);

    CustomerDTO update(Customer artist);

    boolean delete(Long id);
}
