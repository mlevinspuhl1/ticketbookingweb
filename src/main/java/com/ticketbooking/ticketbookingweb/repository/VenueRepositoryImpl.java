/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.repository;

import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.VenueDTO;
import static com.tbcommons.enums.Enums.ResponseEnum.*;
import com.tbcommons.model.Venue;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.servlet.ServletContext;
import javax.validation.ValidationException;

public class VenueRepositoryImpl extends Repository implements VenueRepository {

    private static VenueRepositoryImpl singleinstance = null;

    public static VenueRepositoryImpl getInstance(ServletContext context) {
        if (singleinstance == null) {
            singleinstance = new VenueRepositoryImpl(context);
        }
        return singleinstance;
    }

    private VenueRepositoryImpl(ServletContext context) {
        emf = (EntityManagerFactory) context.getAttribute("emf");
    }

    @Override
    public List<Venue> getAll() {
        List<Venue> venueList = null;
        try {
            em = emf.createEntityManager();

            TypedQuery<Venue> q = em.createQuery("SELECT u from " + Venue.class.getSimpleName() + " u", Venue.class);

            venueList = q.setMaxResults(100).getResultList();

        } catch (Exception ex) {
            Log.error("getAll", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return venueList;
    }

    @Override
    public VenueDTO getById(Long id) {
        try {
            if (id == null) {
                throw new ValidationException("Venue id null");
            }
            em = emf.createEntityManager();
            Venue user = em.find(Venue.class, id);
            return new VenueDTO(SUCCESS, SUCCESS.label, user);
        } catch (ValidationException ex) {
            Log.error("getById", ex);
            return new VenueDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            Log.error("getById", ex);
            return new VenueDTO(FAIL, LOGIN_TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public VenueDTO save(Venue venue) {
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            em.persist(venue);
            tx.commit();

            return new VenueDTO(SUCCESS, SUCCESS.label, venue);
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("save", ex);
            return new VenueDTO(FAIL, TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public VenueDTO update(Venue venue) {

        try {
            if (venue == null) {
                throw new ValidationException("Venue null");
            }
            if (venue.getId() == null) {
                throw new ValidationException("Venue id null");
            }
            VenueDTO dto = getById(venue.getId());

            if (dto == null || dto.getVenue() == null) {
                throw new ValidationException("Venue not found by id:" + venue.getId());
            }

            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            venue = em.merge(venue);
            tx.commit();
            return new VenueDTO(SUCCESS, SUCCESS.label, venue);
        } catch (ValidationException ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("update", ex);
            return new VenueDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("update", ex);
            return new VenueDTO(FAIL, CONTACT_ADMIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean delete(Long id) {
        try {
            VenueDTO dto = getById(id);

            if (dto == null || dto.getVenue() == null) {
                throw new Exception("Venue not found by id:" + id);
            }
            Venue user = dto.getVenue();
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            if (!em.contains(user)) {
                Venue current = em.merge(user);
                em.remove(current);
            } else {
                em.remove(user);
            }
            tx.commit();
            return true;
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("delete", ex);
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
