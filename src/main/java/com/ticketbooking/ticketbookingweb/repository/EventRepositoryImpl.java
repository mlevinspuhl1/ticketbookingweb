/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.repository;

import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.EventDTO;
import static com.tbcommons.enums.Enums.ResponseEnum.*;
import com.tbcommons.model.Event;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.servlet.ServletContext;
import javax.validation.ValidationException;

/**
 *
 * @author
 */
public class EventRepositoryImpl extends Repository implements EventRepository {

    private static EventRepositoryImpl singleinstance = null;

    public static EventRepositoryImpl getInstance(ServletContext context) {
        if (singleinstance == null) {
            singleinstance = new EventRepositoryImpl(context);
        }
        return singleinstance;
    }

    private EventRepositoryImpl(ServletContext context) {
        emf = (EntityManagerFactory) context.getAttribute("emf");
    }

    @Override
    public List<Event> getAll() {
        List<Event> list = null;
        try {
            em = emf.createEntityManager();

            TypedQuery<Event> q = em.createQuery("SELECT e from " + Event.class.getSimpleName() + " e", Event.class);

            list = q.setMaxResults(100).getResultList();

        } catch (Exception ex) {
            Log.error("getAll", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return list;
    }

    @Override
    public EventDTO getById(Long id) {
        try {
            if (id == null) {
                throw new ValidationException("Event id null");
            }
            em = emf.createEntityManager();
            Event event = em.find(Event.class, id);
            return new EventDTO(SUCCESS, SUCCESS.label, event);
        } catch (ValidationException ex) {
            Log.error("getById", ex);
            return new EventDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            Log.error("getById", ex);
            return new EventDTO(FAIL, LOGIN_TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public EventDTO save(Event event) {
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            em.persist(event);
            tx.commit();

            return new EventDTO(SUCCESS, SUCCESS.label, event);
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("save", ex);
            return new EventDTO(FAIL, TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public EventDTO update(Event event) {

        try {
            if (event == null) {
                throw new ValidationException("Event null");
            }
            if (event.getId() == null) {
                throw new ValidationException("Event id null");
            }
            EventDTO dto = getById(event.getId());

            if (dto == null || dto.getEvent() == null) {
                throw new ValidationException("Event not found by id:" + event.getId());
            }

            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            event = em.merge(event);
            tx.commit();
            return new EventDTO(SUCCESS, SUCCESS.label, event);
        } catch (ValidationException ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("update", ex);
            return new EventDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("update", ex);
            return new EventDTO(FAIL, CONTACT_ADMIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean delete(Long id) {
        try {
            EventDTO dto = getById(id);

            if (dto == null || dto.getEvent() == null) {
                throw new Exception("Event not found by id:" + id);
            }
            Event event = dto.getEvent();
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            if (!em.contains(event)) {
                Event current = em.merge(event);
                em.remove(current);
            } else {
                em.remove(event);
            }
            tx.commit();
            return true;
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("delete", ex);
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Event> getByArtistID(Long id) {
        List<Event> list = null;
        try {
            em = emf.createEntityManager();

            TypedQuery<Event> q = em.createQuery("SELECT e from "
                    + Event.class.getSimpleName()
                    + " e where e.artistId.id = :id",
                     Event.class);
            q = q.setParameter("id", id);
            list = q.setMaxResults(100).getResultList();

        } catch (Exception ex) {
            Log.error("getAll", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return list;
    }
}
