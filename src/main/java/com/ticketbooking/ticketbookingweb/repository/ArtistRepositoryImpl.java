/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.repository;

import static com.tbcommons.constant.Constants.*;
import com.tbcommons.dto.ArtistDTO;
import static com.tbcommons.enums.Enums.ResponseEnum.*;
import com.tbcommons.model.Artist;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.servlet.ServletContext;
import javax.validation.ValidationException;

/**
 *
 * @author genef
 */
public class ArtistRepositoryImpl extends Repository implements ArtistRepository {

    private static ArtistRepositoryImpl singleinstance = null;

    public static ArtistRepositoryImpl getInstance(ServletContext context) {
        if (singleinstance == null) {
            singleinstance = new ArtistRepositoryImpl(context);
        }
        return singleinstance;
    }

    private ArtistRepositoryImpl(ServletContext context) {
        emf = (EntityManagerFactory) context.getAttribute("emf");
    }

    @Override
    public List<Artist> getAll() {
        List<Artist> aList = null;
        try {
            em = emf.createEntityManager();

            TypedQuery<Artist> q = em.createQuery("SELECT a from " + Artist.class.getSimpleName() + " a", Artist.class);

            aList = q.setMaxResults(100).getResultList();

        } catch (Exception ex) {
            Log.error("getAll", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return aList;
    }

    @Override
    public ArtistDTO getById(Long id) {
        try {
            if (id == null) {
                throw new ValidationException("Artist id null");
            }
            em = emf.createEntityManager();
            Artist artist = em.find(Artist.class, id);
            return new ArtistDTO(SUCCESS, SUCCESS.label, artist);
        } catch (ValidationException ex) {
            Log.error("getById", ex);
            return new ArtistDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            Log.error("getById", ex);
            return new ArtistDTO(FAIL, LOGIN_TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArtistDTO save(Artist artist) {
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            em.persist(artist);
            tx.commit();

            return new ArtistDTO(SUCCESS, SUCCESS.label, artist);
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("save", ex);
            return new ArtistDTO(FAIL, TRY_AGAIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArtistDTO update(Artist artist) {

        try {
            if (artist == null) {
                throw new ValidationException("Artist is null");
            }
            if (artist.getId() == null) {
                throw new ValidationException("Artist id is null");
            }
            ArtistDTO dto = getById(artist.getId());

            if (dto == null || dto.getArtist() == null) {
                throw new ValidationException("Artist not found by id: " + artist.getId());
            }

            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            artist = em.merge(artist);
            tx.commit();
            return new ArtistDTO(SUCCESS, SUCCESS.label, artist);
        } catch (ValidationException ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("update", ex);
            return new ArtistDTO(FAIL, ex.getMessage());
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("update", ex);
            return new ArtistDTO(FAIL, CONTACT_ADMIN);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean delete(Long id) {
        try {
            ArtistDTO dto = getById(id);

            if (dto == null || dto.getArtist() == null) {
                throw new Exception("Artist not found by id:" + id);
            }
            Artist artist = dto.getArtist();
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            if (!em.contains(artist)) {
                Artist current = em.merge(artist);
                em.remove(current);
            } else {
                em.remove(artist);
            }
            tx.commit();
            return true;
        } catch (Exception ex) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            Log.error("delete", ex);
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
