/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.listener;

import static com.tbcommons.constant.Constants.PERSISTENCE_UNIT_NAME;
import com.ticketbooking.ticketbookingweb.service.SendMail;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Web application lifecycle listener.
 *
 */
public class EntityManagerFactoryListener implements ServletContextListener {

    private static final Logger Log = LogManager.getLogger(SendMail.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            EntityManagerFactory emf
                    = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
            sce.getServletContext().setAttribute("emf", emf);
        } catch (Exception ex) {
            Log.error("contextInitialized", ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            EntityManagerFactory emf
                    = (EntityManagerFactory) sce.getServletContext().getAttribute("emf");
            emf.close();
        } catch (Exception ex) {
            Log.error("contextDestroyed", ex);
        }
    }
}
