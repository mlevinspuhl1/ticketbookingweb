/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb;

import com.tbcommons.dto.TicketDTO;
import com.tbcommons.dto.TicketPaymentDTO;
import com.tbcommons.model.Payment;
import com.tbcommons.model.Ticket;
import com.ticketbooking.ticketbookingweb.repository.TicketRepositoryImpl;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author
 */
@Path("/ticket")
public class TicketResource {

    @Context
    private ServletContext context;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Ticket> getAll() {
        return TicketRepositoryImpl.getInstance(context).getAll();
    }

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TicketDTO save(Ticket ticket) {
        return TicketRepositoryImpl.getInstance(context).save(ticket);
    }

    @POST
    @Path("/saveAll")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TicketDTO saveAll(TicketPaymentDTO dto) {
        return TicketRepositoryImpl.getInstance(context).saveAll(dto);
    }

    @PUT
    @Path("/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TicketDTO update(@PathParam("id") Long id, Ticket ticket) {
        ticket.setId(id);
        return TicketRepositoryImpl.getInstance(context).update(ticket);
    }

    @POST
    @Path("/update/printed")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TicketDTO updatePrintedList(List<Long> ids) {
        return TicketRepositoryImpl.getInstance(context).updatePrintedList(ids);
    }

    @DELETE
    @Path("/remove/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean delete(@PathParam("id") Long id) {
        return TicketRepositoryImpl.getInstance(context).delete(id);
    }
}
