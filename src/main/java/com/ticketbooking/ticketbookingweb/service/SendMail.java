/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.service;

import static com.tbcommons.constant.Constants.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SendMail {

    private static final Logger Log = LogManager.getLogger(SendMail.class);
    private static final String EMAIL = "gencruz1971@gmail.com";
    private static final String PASSWORD = "Cricri@123";
    private static SendMail instance = null;

    public static SendMail getInstance() {
        if (instance == null) {
            instance = new SendMail();
        }
        return instance;
    }

    private SendMail() {
    }

    public boolean SendPlainEmail(String encrypted) {

        try {
            StringTokenizer tokenizer = AuthenticationService.getInstance()
                    .getUserCredential(encrypted);
            final String emailTo = tokenizer.nextToken();
            Properties prop = System.getProperties();

            // Setup mail server
            prop.put("mail.smtp.host", "smtp.gmail.com");
            prop.put("mail.smtp.port", "587");
            prop.put("mail.smtp.auth", "true");
            prop.put("mail.smtp.starttls.enable", "true"); //TLS

            Session session = Session.getInstance(prop,
                    new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(EMAIL, PASSWORD);
                }
            });

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(EMAIL));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(emailTo)
            );
            message.setSubject("Ticket Booking User Registration");
            String token = URL_API + "/" + ACTIVE_USER_EMAIL_PATH + "?key=" + encrypted;

            message.setContent("<p>Dear User,</p>"
                    + "<p>Your account was registered!</p>"
                    + "<p>Please click in this link to complete the regitartion process</p>"
                    + "<p><a href=\"" + token + "\">" + token + "</a></p>"
                    + "<p>Kind Regards</p><p>Ticket Booking Team</p>", "text/html");
            
            Transport.send(message);

            System.out.println("Done");

        } catch (Exception e) {
            Log.error("SendPlainEmail", e);
            return false;
        }
        return true;
    }

//    public SendMail(String email, String subject, String messageBody, String filepath, String filename) {
////        this.context = context;
//        this.email = email;
//        this.subject = subject;
//        this.messageBody = messageBody;
//        this.filepath = filepath;
//        this.filename = filename;
//    }
}
