/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb;

import com.tbcommons.dto.EventDTO;
import com.tbcommons.model.Event;
import com.ticketbooking.ticketbookingweb.repository.EventRepositoryImpl;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 *
 */
@Path("/event")
public class EventResource {

    @Context
    private ServletContext context;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> getAll() {
        return EventRepositoryImpl.getInstance(context).getAll();
    }

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public EventDTO saveUser(Event event) {
        return EventRepositoryImpl.getInstance(context).save(event);
    }

    @PUT
    @Path("/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public EventDTO update(@PathParam("id") Long id, Event event) {
        event.setId(id);
        return EventRepositoryImpl.getInstance(context).update(event);
    }

    @DELETE
    @Path("/remove/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean deleteUser(@PathParam("id") Long id) {
        return EventRepositoryImpl.getInstance(context).delete(id);
    }
        @GET
    @Path("artist/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> getByArtistID(@PathParam("id") Long id) {
        return EventRepositoryImpl.getInstance(context).getByArtistID(id);
    }
}
