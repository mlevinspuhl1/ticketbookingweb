/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb;

import com.tbcommons.dto.UserDTO;
import com.tbcommons.model.Artist;
import com.tbcommons.model.Customer;
import com.tbcommons.model.Employee;
import com.tbcommons.model.User;
import com.ticketbooking.ticketbookingweb.repository.UserRepositoryImpl;
import com.ticketbooking.ticketbookingweb.service.SendMail;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 */
@Path("/user")
public class UserResource {

    @Context
    private ServletContext context;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> getUser() {
        return UserRepositoryImpl.getInstance(context).getAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserDTO getUserById(@PathParam("id") Long id) {
        return UserRepositoryImpl.getInstance(context).getById(id);
    }

    @GET
    @Path("/email")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean sendEmail(@QueryParam("key") String encrypted) {
        return SendMail.getInstance().SendPlainEmail(encrypted);
    }

    @GET
    @Path("/email/active")
    @Produces(MediaType.TEXT_HTML)
    public String activeUserByEmail(@QueryParam("key") String encrypted) {
        return UserRepositoryImpl.getInstance(context).activeUserByEmail(encrypted);
    }

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserDTO saveUser(User user) {

        return UserRepositoryImpl.getInstance(context).save(user);
    }

    @DELETE
    @Path("/remove/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean deleteUser(@PathParam("id") Long id) {
        return UserRepositoryImpl.getInstance(context).delete(id);
    }

    @PUT
    @Path("/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserDTO update(@PathParam("id") Long id, User user) {
        user.setId(id);
        return UserRepositoryImpl.getInstance(context).update(user);
    }

    @PUT
    @Path("/employee/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserDTO updateEmployee(@PathParam("id") Long id, Employee emp) {
        return UserRepositoryImpl.getInstance(context).updateEmployee(id, emp);
    }

    @PUT
    @Path("/customer/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserDTO updateCustomer(@PathParam("id") Long id, Customer customer) {
        return UserRepositoryImpl.getInstance(context).updateCustomer(id, customer);
    }

    @PUT
    @Path("/artist/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserDTO updateArtist(@PathParam("id") Long id, Artist artist
    ) {
        return UserRepositoryImpl.getInstance(context).updateArtist(id, artist);
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public UserDTO login(String encrypted) {
        return UserRepositoryImpl.getInstance(context).login(encrypted);
    }
}
