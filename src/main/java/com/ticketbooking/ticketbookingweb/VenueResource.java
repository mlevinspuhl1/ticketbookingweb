/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb;

import com.tbcommons.dto.VenueDTO;
import com.tbcommons.model.Venue;
import com.ticketbooking.ticketbookingweb.repository.VenueRepositoryImpl;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 *
 */
@Path("/venue")
public class VenueResource {

    @Context
    private ServletContext context;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Venue> getAll() {
        return VenueRepositoryImpl.getInstance(context).getAll();
    }

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public VenueDTO save(Venue venue) {
        return VenueRepositoryImpl.getInstance(context).save(venue);
    }

    @PUT
    @Path("/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public VenueDTO update(@PathParam("id") Long id, Venue venue) {
        venue.setId(id);
        return VenueRepositoryImpl.getInstance(context).update(venue);
    }

    @DELETE
    @Path("/remove/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean delete(@PathParam("id") Long id) {
        return VenueRepositoryImpl.getInstance(context).delete(id);
    }

}
