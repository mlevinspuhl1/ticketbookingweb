/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb.filter;

import static com.tbcommons.constant.Constants.ACTIVE_USER_EMAIL_PATH;
import com.ticketbooking.ticketbookingweb.repository.UserRepositoryImpl;
import com.ticketbooking.ticketbookingweb.service.AuthenticationService;
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;

public class RestAuthenticationFilter implements javax.servlet.Filter {

    private static final org.apache.logging.log4j.Logger Log = LogManager.getLogger(UserRepositoryImpl.class);
    public static final String AUTHENTICATION_HEADER = "Authorization";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain filter) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            String authCredentials = httpServletRequest
                    .getHeader(AUTHENTICATION_HEADER);
            String scheme = httpServletRequest.getScheme();             // http
            String serverName = httpServletRequest.getServerName();     // hostname.com
            int serverPort = httpServletRequest.getServerPort();        // 80
            String contextPath = httpServletRequest.getContextPath();   // /mywebapp
            String servletPath = httpServletRequest.getServletPath();   // /servlet/MyServlet
            String pathInfo = httpServletRequest.getPathInfo();         // /a/b;c=123
            String queryString = httpServletRequest.getQueryString();          // d=789

            boolean authenticationStatus = false;
            if (pathInfo.equals("/"+ACTIVE_USER_EMAIL_PATH)
                    && queryString.contains("key=")) {
                authenticationStatus = true;
            } else {

                // better injected
                try {
                    authenticationStatus = AuthenticationService.getInstance()
                            .authenticate(authCredentials);
                } catch (Exception ex) {
                    Log.error("doFilter", ex);
                }
            }
            if (authenticationStatus) {
                filter.doFilter(request, response);
            } else {
                if (response instanceof HttpServletResponse) {
                    HttpServletResponse httpServletResponse = (HttpServletResponse) response;
                    httpServletResponse
                            .setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }
            }
        }
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }
}
