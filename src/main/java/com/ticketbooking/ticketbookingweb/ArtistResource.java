/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb;

import com.tbcommons.dto.ArtistDTO;
import com.tbcommons.model.Artist;
import com.ticketbooking.ticketbookingweb.repository.ArtistRepositoryImpl;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * 
 */
@Path("/artist")
public class ArtistResource {

    @Context
    private ServletContext context;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Artist> getAll() {
        return ArtistRepositoryImpl.getInstance(context).getAll();
    }

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArtistDTO save(Artist artist) {
        return ArtistRepositoryImpl.getInstance(context).save(artist);
    }

    @PUT
    @Path("/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArtistDTO update(@PathParam("id") Long id, Artist artist) {
        artist.setId(id);
        return ArtistRepositoryImpl.getInstance(context).update(artist);
    }

    @DELETE
    @Path("/remove/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean delete(@PathParam("id") Long id) {
        return ArtistRepositoryImpl.getInstance(context).delete(id);
    }

}
