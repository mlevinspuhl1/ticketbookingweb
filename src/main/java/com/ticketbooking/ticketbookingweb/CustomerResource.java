/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketbooking.ticketbookingweb;

import com.tbcommons.dto.CustomerDTO;
import com.tbcommons.model.Customer;
import com.ticketbooking.ticketbookingweb.repository.CustomerRepositoryImpl;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author
 */
@Path("/customer")
public class CustomerResource {

    @Context
    private ServletContext context;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Customer> getAll() {
        return CustomerRepositoryImpl.getInstance(context).getAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public CustomerDTO getCustomerById(@PathParam("id") Long id) {
        return CustomerRepositoryImpl.getInstance(context).getById(id);
    }

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CustomerDTO save(Customer customer) {
        return CustomerRepositoryImpl.getInstance(context).save(customer);
    }

    @PUT
    @Path("/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CustomerDTO update(@PathParam("id") Long id, Customer customer) {
        customer.setId(id);
        return CustomerRepositoryImpl.getInstance(context).update(customer);
    }

    @DELETE
    @Path("/remove/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean delete(@PathParam("id") Long id) {
        return CustomerRepositoryImpl.getInstance(context).delete(id);
    }
}
